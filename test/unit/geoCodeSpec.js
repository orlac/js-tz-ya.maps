
require('../../src/app/controllers/MapCtrl.js');
require('../../src/app/services/Point.js');
require('../../src/app/mock/GeoCode.js');
var _ = require('../../bower_components/lodash/lodash.min.js');


describe('geocode', function () {

    var pointFactory, scope, MapCtrl, geoCoder;
    

    beforeEach(function () {
        
        

        angular.module('yaMap', []);
        angular.mock.module('pointModule');
        angular.mock.module('geoCodeModule');
        angular.mock.module('MapModule');
        

        inject(function ($rootScope, $controller, $injector) {
                                 

            pointFactory = $injector.get('pointFactory');
            geoCoder = $injector.get('geoCoder');
            scope = $rootScope.$new();
            MapCtrl = $controller('MapCtrl', {$scope: scope, pointFactory: pointFactory, geoCoder: geoCoder});
        });
    });

    var _e = {
        get: function(){
            return {
                geometry: {
                    getCoordinates: function(){ return [10, 10] }
                }
            }
        }
    }

    var _submit = function(ListCtrl, name){
        ListCtrl.inputPoint = name;
        ListCtrl.submit();        
    }

    it('geocode', function () {
        console.info('run geocode');
        pointFactory.addNamePoint('point-0');
        scope.$digest();
        console.log('pointFactory.points: ', pointFactory.points);
        expect(pointFactory.points.length).toEqual(1);
        expect( typeof pointFactory.points[0].geoInfo.geometry == 'object').toEqual(true);
                
    });

    it('bad geocode', function () {
        console.info('bad geocode');
        pointFactory.addNamePoint('point-10');
        scope.$digest();
        console.log('pointFactory.points: ', pointFactory.points);
        expect(pointFactory.points.length).toEqual(0);
    });

    it('make route', function () {
        console.info('make route');
        pointFactory.addNamePoint('point-0');
        pointFactory.addNamePoint('point-1');
        pointFactory.addNamePoint('point-2');
        scope.$digest();
        console.log('pointFactory.route: ', pointFactory.route);
        expect( typeof pointFactory.route.geometry == 'object').toEqual(true);
        expect( pointFactory.route.geometry.type).toEqual('LineString');
        expect( pointFactory.route.geometry.coordinates.length).toEqual(3);
        expect( pointFactory.route.geometry.coordinates).toEqual([[0, 0], [1, 1], [2, 2]]);
        
    });

    it('remake route', function () {
        console.info('make route');
        pointFactory.addNamePoint('point-0');
        pointFactory.addNamePoint('point-1');
        pointFactory.addNamePoint('point-2');
        scope.$digest();
        pointFactory.points = _.shuffle(pointFactory.points);
        pointFactory.resort(); 
        scope.$digest();

        var _coords = _.map(pointFactory.points, function(point){
            return point.geoInfo.geometry.coordinates;
        });

        console.log('_coords: ', _coords);
        
        expect( pointFactory.route.geometry.coordinates).toEqual(_coords);
        
    });

    it('point move', function () {
        console.info('point move');
        pointFactory.addNamePoint('point-0');
        pointFactory.addNamePoint('point-1');
        pointFactory.addNamePoint('point-2');
        scope.$digest();
        
        MapCtrl.pointMove(_e, 0);

        console.log('pointFactory.points[0].geoInfo.geometry.coordinates: ', pointFactory.points[0].geoInfo.geometry.coordinates);
        expect( pointFactory.points[0].geoInfo.geometry.coordinates).toEqual([10, 10]);
        expect( pointFactory.route.geometry.coordinates).toEqual([[10, 10], [1, 1], [2, 2]]);
        
    });

    it('ballon open', function () {
        console.info('ballon open');
        MapCtrl.init('загрузка');
        pointFactory.addNamePoint('point-0');
        pointFactory.addNamePoint('point-1');
        pointFactory.addNamePoint('point-2');
        scope.$digest();
        
        
        expect( pointFactory.points[0].geoInfo.properties.balloonContent).toEqual('загрузка');
        MapCtrl.balloonOpen(_e, 0);
        scope.$digest();
        expect( pointFactory.points[0].geoInfo.properties.balloonContent).toEqual('point-10');
        
    });

});