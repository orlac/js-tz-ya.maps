
require('../../src/app/controllers/ListCtrl.js');
require('../../src/app/services/Point.js');


describe('list', function () {

    var pointFactory, scope, ListCtrl;
    

    beforeEach(function () {
        
        angular.module('ng-sortable',[]);
        angular.module('xeditable',[]);

        angular.mock.module('pointModule');
        angular.mock.module('listModule');
        

        inject(function ($rootScope, $controller, $injector) {
                                 

            pointFactory = $injector.get('pointFactory');
            scope = $rootScope.$new();
            ListCtrl = $controller('ListCtrl', {$scope: scope, pointFactory: pointFactory});
        });
    });

    var _submit = function(ListCtrl, name){
        ListCtrl.inputPoint = name;
        ListCtrl.submit();        
    }

    it('init', function () {
        
        console.log('init', pointFactory.points);
        expect(pointFactory.points.length).toEqual(0);
                
    });



    it('add', function () {

        _submit(ListCtrl, 'point-0');
        
        console.log('add', pointFactory.points);

        expect(pointFactory.points.length).toEqual(1);
        expect(pointFactory.points[0].name).toEqual('point-0');
        expect(pointFactory.points[0].geoInfo).toEqual(null);
        expect(ListCtrl.inputPoint).toEqual(null);
    });


    it('delete', function () {

        _submit(ListCtrl, 'point-1');  

        ListCtrl.delete(1);
        console.log('delete 1', pointFactory.points);
        expect(pointFactory.points.length).toEqual(1);
        ListCtrl.delete(0);
        console.log('delete 0', pointFactory.points);
        expect(pointFactory.points.length).toEqual(0);
        
    });

});