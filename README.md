### install ###

* git clone https://orlac@bitbucket.org/orlac/js-tz-ya.maps.git
* cd js-tz-ya.maps
* npm install && bower install
* gulp build && node app.js
* [https://127.0.0.1:3000](https://127.0.0.1:3000)

### tests ###

* cd test
* npm install
* npm test
