'use strict';

var ctrl = function($scope, $q, pointFactory, geoCoder){
	
	this.pointFactory = pointFactory;
	this.balloonContent = null;
	var map = null;
	var self = this;
	
	var geoCodePoints = function(){
		

		var deferred = $q.defer(); 
		var isFind = _.find(pointFactory.points, function(item, index){
			return _.isNull(item.geoInfo)
		});
		if(!isFind){
			deferred.resolve();	
		}else{

			var _fns = [];

			_.each( pointFactory.points, function(item, index) {
				_fns.push(function(cb){
					geoCoder.geoCode( item.name, map )
						.then(function(geoInfo){
							if(!geoInfo){
								//если не нашли координаты, удалить точку
								// pointFactory.data.splice(index, 1);
								pointFactory.removePoint(index);
							}else{
								console.log('geoInfo by '+item.name, geoInfo);
								geoInfo.geometry.type = 'Point';
								geoInfo.properties = {
									balloonContent: self.balloonContent
								};
								pointFactory.setGeoByIndex(index, geoInfo);
							}
							cb();	
						})
						.catch(cb)			
				} );
			});

			async.parallel(_fns, function(err){
				if(err) return deferred.reject();	
				deferred.resolve();
			})
		}
		return deferred.promise;
	}

	var route = null;
	var makeRoute = function(){
		var coords = _.map(pointFactory.points, function(point){
			return point.geoInfo? point.geoInfo.geometry.coordinates : false;
		} );
		coords = _.filter(coords, function(item){ 
			return item !== false; 
		} );

		console.log('makeRoute', pointFactory.points.length, coords)
		pointFactory.route = {
			geometry: {
	            type: 'LineString',
	            coordinates: coords
	        }
		};
	}

	this.init = function(balloonContent){
		this.balloonContent = balloonContent;
	}

	this.mapAfterInit = function(_map){
		map = _map;
	}

	this.pointMove = function(e, index){
		var coords = e.get('target').geometry.getCoordinates();
		pointFactory.points[index].geoInfo.geometry.coordinates = coords;
		makeRoute();
		console.log('pointMove', coords, index);
	}

	this.balloonOpen = function(e, index){
		var coords = e.get('target').geometry.getCoordinates();
		pointFactory.points[index].geoInfo.properties.balloonContent = self.balloonContent;
		geoCoder.geoCode( coords, map )
			.then(function(geoInfo){
				pointFactory.points[index].geoInfo.properties.balloonContent = geoInfo.properties.balloonContent;
			});	
	}

	$scope.$watch( function(){
			return pointFactory.isResort;
		}, function(newVal, oldVal){
				makeRoute();
		},
		true );

	$scope.$watch( function(){
			return pointFactory.isChanged;
		}, function(newVal, oldVal){
				geoCodePoints().then(makeRoute);
		},
		true );
};

angular.module('MapModule', ['pointModule', 'geoCodeModule', 'yaMap'])
	.controller('MapCtrl', [
		'$scope'
		, '$q'
		, 'pointFactory'
		, 'geoCoder'
		, ctrl
	]);