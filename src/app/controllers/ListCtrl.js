'use strict';

var ctrl = function($scope, pointFactory){
	
	this.pointFactory = pointFactory;
	this.inputPoint = null;

	var self = this;


	this.submit = function(){
		this.pointFactory.addNamePoint(this.inputPoint);
		this.inputPoint = null;
	}

	this.delete = function(index){
		this.pointFactory.removePoint(index);
	}

	this.onEndSort = function(obj){
		// console.log(obj);
		self.pointFactory.resort();
	}

};

var ng = angular.module('listModule', ['ng-sortable', 'pointModule', 'xeditable']);
// ng.run(['editableOptions', function(editableOptions) {
//   editableOptions.theme = 'bs3'; // bootstrap3 theme. Can be also 'bs2', 'default'
// }]);
ng.controller('ListCtrl', [
		'$scope'
		, 'pointFactory'
		, ctrl
	]);

	