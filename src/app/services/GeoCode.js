'use strict';

var ng = angular.module('geoCodeModule', []);


ng.service('geoCoder', ['$q', function($q){
  


	var geoCode = function(geocodeQuery, map){

		var deferred = $q.defer();

		var opts = (map)? { boundedBy: map.getBounds() } : null;

		ymaps.geocode(geocodeQuery, opts).then(function (res) {
			var geoObject = res.geoObjects.get(0);
			var _res = null;
			if(geoObject){
				_res = {
            		geometry:{
	                    coordinates: geoObject.geometry.getCoordinates()
	                },
	                properties: {
	                	balloonContent: geoObject.properties.get('name')
	                }	
            	}
			}
            
        	deferred.resolve(_res);    
        });

		return deferred.promise;
	}

	return {
		geoCode: geoCode
	}
}]);