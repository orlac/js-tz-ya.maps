'use strict';

var pointModule = angular.module('pointModule', []);


pointModule.factory('pointFactory', function(){



	this.data = [];
	this.addedPoint = null;
	this.points = [];
	this.route = {
		"geometry":{
			"type":"LineString",
			"coordinates":[
				// [37.175961,55.840356],
				// [37.620393,55.75396]
			]
		}
	} ;

	// change propetries
	
	// added new
	// this.isNew = false;
	
	// resort points
	this.isResort = false;

	// change points
	this.isChanged = false;


	var _name_geo = {}; 

	this.addNamePoint = function(name){
		// set isNew
		this.points.push( {
				name: name,
				geoInfo: null
			} );
		this.isChanged = !this.isChanged;
	}

	this.changedPoint = function(i){
		// set isNew
		this.points[i].geoInfo = null;
		this.isChanged = !this.isChanged;
	}

	this.resort = function(){
		this.isResort = !this.isResort;		
	}

	this.removePoint = function(index){
		// set isChange
		// this.data.splice(index, 1);
		this.points.splice(index, 1);
		this.isChanged = !this.isChanged;
	}

	this.setGeoByIndex = function(index, geoInfo){
		this.points[index].geoInfo = geoInfo;
		// var _i = _.indexOf(this.data, name);
		// if(this.points[_i]){
		// 	this.points[_i] = geoInfo;
		// }else{
		// 	this.points.push( {
		// 		name: name,
		// 		geoInfo: geoInfo	
		// 	} )		
		// }

		// _name_geo[name] = geoInfo;
	}


	this.remakePoints = function(){
		this.points = [];
		var self = this;
		_.each(this.data, function(item, i){
			var _geo = _name_geo[ item ];
			self.points.push(_geo);
		});
		// for(var i in this.data){
		// 	var _geo = _name_geo[ this.data[i] ];
		// 	this.points.push(_geo);
		// }
	}


	this.removePointByName = function(name){
		this.removePoint(  _.indexOf(this.data, name) );
	}

	return this;
  
})