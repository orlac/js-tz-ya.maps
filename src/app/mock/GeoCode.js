'use strict';

var ng = angular.module('geoCodeModule', []);

ng.value('geoObjects', {
	'point-0': {
		geometry: {
			coordinates: [0, 0]
		},
		properties: {
        	balloonContent: 'point-0'
        }
	},

	'point-1': {
		geometry: {
			coordinates: [1, 1]
		},
		properties: {
        	balloonContent: 'point-1'
        }
	},

	'point-2': {
		geometry: {
			coordinates: [2, 2]
		},
		properties: {
        	balloonContent: 'point-2'
        }
	},
});

ng.service('geoCoder', ['$q', 'geoObjects', function($q, geoObjects){
  


	var geoCode = function(geoName){
		var deferred = $q.defer();  
		var res = null;
		if( typeof geoName == 'object' /*&& geoName == [10, 10]*/ ){
			res = {
				geometry: {
					coordinates: [10, 10]
				},
				properties: {
		        	balloonContent: 'point-10'
		        }
		    };
		}
		else if(geoObjects[geoName]){
			res = geoObjects[geoName];
		}
		// setTimeout(function(){
		// 	deferred.resolve(res);
		// }, 0);
		deferred.resolve(res);
		
		return deferred.promise;
	}



	return {

		geoCode: geoCode



	}
}]);