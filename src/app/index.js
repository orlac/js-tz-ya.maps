'use strict';
(function(window){

	var bower_components = '../bower_components';

	window.jQuery = window.$ = require('../../bower_components/jquery/dist/jquery.min.js');
	require('angular');
	require('angular-touch');
	require('angular-mocks');
	// require('ng-sortable');
	window._ = require('../../bower_components/lodash/lodash.min.js');
	window.async = require('../../bower_components/async/dist/async.min.js');

	
	window.Sortable = require('../../other_components/Sortable/Sortable.js');
	require('../../other_components/Sortable/ng-sortable.js');
	
	require('../../other_components/ya-map-2.2.js');

	
	require('../../bower_components/angular-xeditable/dist/css/xeditable.css');
	require('../../bower_components/angular-xeditable/dist/js/xeditable.js');
	

	require('../../bower_components/angular/angular-csp.css');
	require('../../bower_components/font-awesome/css/font-awesome.min.css');
	require('../../bower_components/bootstrap/dist/css/bootstrap.min.css');
	require('../../bower_components/bootstrap/dist/js/bootstrap.min.js');
	

	require('./services/Point.js');
	// require('./mock/GeoCode.js');
	require('./services/GeoCode.js');
	require('./controllers/ListCtrl.js');
	require('./controllers/MapCtrl.js');
	

	var ng = angular.module('application1', [
		'ngTouch'
		,'listModule'
		,'MapModule'
	]);

	angular.element(document).ready(function() {
	    angular.bootstrap(document, [ng.name]);
	});


})(window);




